package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.controller.EmployeeController;
import AngajatiApp.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class EmployeeMockTest {
    private Employee employee;
    EmployeeMock employeeMock;

    @BeforeEach
    void Setup() {
        employee = new Employee();
        employee.setId(1);
        employee.setCnp("1870124214020");
        employee.setFirstName("Ion");
        employee.setLastName("Popescu");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2500.00);
        employeeMock = new EmployeeMock();
        System.out.println("setup");
    }

    @AfterEach
    void tearDown() {
        System.out.println("tearDown");
    }

    @Test
    void addEmployeeTC1() {
        //1	Ion	Popescu	1870124314020	ASISTENT	2500
        int nrEmployee = employeeMock.getEmployeeList().size();
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(nrEmployee+1, employeeMock.getEmployeeList().size());
        assertEquals(true, result);
    }

    @Test
    void addEmployeeTC2() {
        //0	Marius	Pop	2880131314021	LECTURER	5500
        employee.setId(0);
        employee.setFirstName("Marius");
        employee.setLastName("Pop");
        employee.setCnp("2880131314021");
        employee.setFunction(DidacticFunction.LECTURER);
        employee.setSalary(5500d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(false, result);
    }

    @Test
    void addEmployeeTC3() {
        //3	Vasile	null	1980131314021	TEACHER	3300
        employee.setId(3);
        employee.setFirstName("Vasile");
        employee.setLastName(null);
        employee.setCnp("1980131314021");
        employee.setFunction(DidacticFunction.TEACHER);
        employee.setSalary(3300d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(false, result);
    }
    @Test
    void addEmployeeTC4() {
        //4	Liviu		2780131314021	TEACHER	1500
        employee.setId(4);
        employee.setFirstName("Liviu");
        employee.setLastName("");
        employee.setCnp("2780131314021");
        employee.setFunction(DidacticFunction.TEACHER);
        employee.setSalary(1500d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(false, result);
    }

    @Test
    void addEmployeeTC5() {
        //5	Nae	Har	15201313140	CONFERENTIAR	1600
        employee.setId(5);
        employee.setFirstName("Nae");
        employee.setLastName("Har");
        employee.setCnp("15201313140");
        employee.setFunction(DidacticFunction.CONFERENTIAR);
        employee.setSalary(1600d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(false, result);
    }

    @Test
    void addEmployeeTC6() {
        //-1 Tudor     2990131314021 CONFERENTIAR 1800
        employee.setId(-1);
        employee.setFirstName("Tudor");
        employee.setLastName("");
        employee.setCnp("2990131314021");
        employee.setFunction(DidacticFunction.CONFERENTIAR);
        employee.setSalary(1800d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(false, result);
    }

    @Test
    void addEmployeeTC7() {
        //7	petru	null	178013131	ASISTENT	2000
        employee.setId(7);
        employee.setFirstName("petru");
        employee.setLastName(null);
        employee.setCnp("178013131");
        employee.setFunction(DidacticFunction.ASISTENT);
        employee.setSalary(2000d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(false, result);
    }

    @Test
    void addEmployeeTC8() {
        //6	Sorin	Gal	1870124314022	LECTURER	3200
        employee.setId(6);
        employee.setFirstName("Sorin");
        employee.setLastName("Gal");
        employee.setCnp("1870124314022");
        employee.setFunction(DidacticFunction.LECTURER);
        employee.setSalary(1100d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(false, result);
    }

    @Test
    void addEmployeeTC9() {
        //  2	Ion	Popescu	1870124314020	ASISTENT	2500
        employee.setId(2);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(true, result);
    }

    @Test
    void addEmployeeTC10() {
        //  10	Ion	P	1870124314020	ASISTENT	2500
        employee.setId(10);
        employee.setLastName("P");
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(true, result);
    }

    @Test
    void addEmployeeTC11() {
        //  11	Ion	Po	1870124314020	ASISTENT	2500
        employee.setId(11);
        employee.setLastName("Po");
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(true, result);
    }

    @Test
    void addEmployeeTC12() {
        //15 Ion Popescu	1870124314020	ASISTENT	2500
        employee.setId(15);
        int nrEmployee = employeeMock.getEmployeeList().size();
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(nrEmployee+1, employeeMock.getEmployeeList().size());
        assertEquals(true, result);
    }

    @Test
    void addEmployeeTC13() {
        //20	Nae	Har	15201313140401	CONFERENTIAR	1600
        employee.setId(20);
        employee.setFirstName("Nae");
        employee.setLastName("Har");
        employee.setCnp("15201313140401");
        employee.setFunction(DidacticFunction.CONFERENTIAR);
        employee.setSalary(1600d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(false, result);
    }

    @Test
    void addEmployeeTC14() {
        //6	Sorin	Gal	1870124314022	LECTURER	3200
        employee.setId(16);
        employee.setFirstName("Sorin");
        employee.setLastName("Gal");
        employee.setCnp("1870124314022");
        employee.setFunction(DidacticFunction.LECTURER);
        employee.setSalary(1199d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(false, result);
    }

    @Test
    void addEmployeeTC15() {
        //6	Sorin	Gal	1870124314022	LECTURER	3200
        employee.setId(18);
        employee.setFirstName("Sorin");
        employee.setLastName("Gal");
        employee.setCnp("1870124314022");
        employee.setFunction(DidacticFunction.LECTURER);
        employee.setSalary(1200d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(true, result);
    }

    @Test
    void addEmployeeTC16() {
        //6	Sorin	Gal	1870124314022	LECTURER	3200
        employee.setId(19);
        employee.setFirstName("Sorin");
        employee.setLastName("Gal");
        employee.setCnp("1870124314022");
        employee.setFunction(DidacticFunction.LECTURER);
        employee.setSalary(1201d);
        boolean result = employeeMock.addEmployee(employee);
        assertEquals(true, result);
    }

    @Test
    void modifyEmployeeFunction() {
        boolean result = employeeMock.modifyEmployeeFunction(new Employee ("Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d),  DidacticFunction.LECTURER);
        assertEquals(true, result);

    }

    @Test
    void modifyEmployeeFunction2() {
        boolean result = employeeMock.modifyEmployeeFunction(null,  DidacticFunction.LECTURER);
        assertEquals(false, result);

    }
}
package AngajatiApp.model;

import AngajatiApp.controller.DidacticFunction;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


import java.util.concurrent.TimeUnit;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmployeeTest extends TestCase {

    public void testConstructor() {
        Employee e = new Employee();
        assert(e.getCnp().equals(""));
    }

    @ParameterizedTest
    @ValueSource(doubles = { 1111.1, 222.44 })
    public void testSetSalary(double argument) {
        // Arrange
        Employee e = new Employee("Dan","Chendrean","1870124314020", DidacticFunction.ASISTENT, argument);

        // Act
        Double firstName = e.getSalary();

        // Assert
        assertEquals(argument, firstName);
    }

    public void testGetCnp() {
        // Arrange
        Employee e = new Employee("Dan","Chendrean","1870124314020", DidacticFunction.ASISTENT, 150.00);

        // Act
        String cnp = e.getCnp();

        // Assert
        assertEquals(13, cnp.length());
        assertEquals("1870124314020", cnp);
    }

    public void testSetCnp() {
        // Arrange
        Employee e = new Employee();

        // Act
        e.setCnp("12345");

        // Assert
        assertEquals("12345", e.getCnp());
    }

    public void testGetFirstName() {
        // Arrange
        Employee e = new Employee("Dan","Chendrean","1870124314020", DidacticFunction.ASISTENT, 150.00);

        // Act
        String firstName = e.getFirstName();

        // Assert
        assertEquals("Dan", firstName);
    }

    public void testGetId() {
        // Arrange
        Employee e = new Employee();
        assertEquals(0, e.getId());
    }

    public void testSetId() {
        // Arrange
        Employee e = new Employee();
        // Act
        e.setId(5);
        assertEquals(5, e.getId());
    }

    @Order(1)
    @Timeout(value = 100, unit = TimeUnit.MILLISECONDS)
    public void testTestEquals() throws InterruptedException {
        Employee e1 = new Employee();
        Employee e2 = new Employee();

        assertTrue(e1.equals(e2));
    }



}